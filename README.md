# README #

## Tasks
  - use real api as offer source http://www.qarson.fr.rc.e-d-p.net/api/offers_list/0
  - our client can't see full source api response
     - extract fields :
       - make_name
       - model_name
       - model_body_name 
       - colour_type_name
       - availability_offer_label
       - offer_gross_price
       - discount_money
    - keep json / xml response format
    - full list collected from paginated response http://www.qarson.fr.rc.e-d-p.net/api/offers_list/0?page=10
  - add car image to results - relative path for xml
  - discuss / plan
    - how to filter full list of cars
    - how to add to each car status information (available from different api) with possibility to filter by this information